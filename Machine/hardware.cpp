#include <SPI.h>
#include <Wire.h>
#include <U8g2lib.h>
#include <KeyDetector.h>
#include <AccelStepper.h>
#include <GEM_u8g2.h>
#include <U8g2lib.h>
#include <Adafruit_NAU7802.h>
#define USE_TIMER_3 true
#include "TimerInterrupt.h"

#include "hardware.h"

bool secondaryPressed = false;

Key keys[] = {{KEY_A, ROTARY_EN1}, {KEY_C, ROTARY_BUTTON}, {KEY_D, STOP_BUTTON},
              {KEY_E, END_STOP_1}, {KEY_F, END_STOP_2}};
KeyDetector myKeyDetector(keys, sizeof(keys)/sizeof(Key), /* debounceDelay= */ 5, /* analogThreshold= */ 16, /* pullup= */ true);

#define USEDRIVER 1
// Stepper Motor 1 (RAMPS X)
#define SM_1_STEP_PIN       54
#define SM_1_DIR_PIN        55
#define SM_1_ENABLE_PIN     38
AccelStepper SM_1(USEDRIVER, SM_1_STEP_PIN, SM_1_DIR_PIN);
// Stepper Motor 2  (RAMPS Y)
#define SM_2_STEP_PIN       60
#define SM_2_DIR_PIN        61
#define SM_2_ENABLE_PIN     56
AccelStepper SM_2(USEDRIVER, SM_2_STEP_PIN, SM_2_DIR_PIN);

Adafruit_NAU7802 nau;

bool shouldReadNAU = false;
int currentNAU = 0;

U8G2_ST7920_128X64_1_SW_SPI u8g2(U8G2_R0, LCD_CLOCK, LCD_DATA, LCD_CS);

void setShouldReadNAU() {
  shouldReadNAU = true;
}

void updateNAU() {
  if (shouldReadNAU) {
    currentNAU = nau.read();
    shouldReadNAU = false;
    Serial.print("Read "); Serial.println(currentNAU);
  }
}

void setupHardware() {
  pinMode(ROTARY_EN1, INPUT_PULLUP);
  pinMode(ROTARY_EN2, INPUT_PULLUP);
  pinMode(ROTARY_BUTTON, INPUT_PULLUP);
  pinMode(STOP_BUTTON, INPUT_PULLUP);
  pinMode(END_STOP_1, INPUT_PULLUP);
  pinMode(END_STOP_2, INPUT_PULLUP);

  Serial.begin(115200);
  if (! nau.begin()) {
    Serial.println("Failed to find NAU7802");
  }
  Serial.println("Found NAU7802");
  nau.setLDO(NAU7802_3V0);
  switch (nau.getLDO()) {
    case NAU7802_4V5:  Serial.println("4.5V"); break;
    case NAU7802_4V2:  Serial.println("4.2V"); break;
    case NAU7802_3V9:  Serial.println("3.9V"); break;
    case NAU7802_3V6:  Serial.println("3.6V"); break;
    case NAU7802_3V3:  Serial.println("3.3V"); break;
    case NAU7802_3V0:  Serial.println("3.0V"); break;
    case NAU7802_2V7:  Serial.println("2.7V"); break;
    case NAU7802_2V4:  Serial.println("2.4V"); break;
    case NAU7802_EXTERNAL:  Serial.println("External"); break;
  }
  nau.setGain(NAU7802_GAIN_16);
  switch (nau.getGain()) {
    case NAU7802_GAIN_1:  Serial.println("1x"); break;
    case NAU7802_GAIN_2:  Serial.println("2x"); break;
    case NAU7802_GAIN_4:  Serial.println("4x"); break;
    case NAU7802_GAIN_8:  Serial.println("8x"); break;
    case NAU7802_GAIN_16:  Serial.println("16x"); break;
    case NAU7802_GAIN_32:  Serial.println("32x"); break;
    case NAU7802_GAIN_64:  Serial.println("64x"); break;
    case NAU7802_GAIN_128:  Serial.println("128x"); break;
  }
  nau.setRate(NAU7802_RATE_10SPS);
  switch (nau.getRate()) {
    case NAU7802_RATE_10SPS:  Serial.println("10 SPS"); break;
    case NAU7802_RATE_20SPS:  Serial.println("20 SPS"); break;
    case NAU7802_RATE_40SPS:  Serial.println("40 SPS"); break;
    case NAU7802_RATE_80SPS:  Serial.println("80 SPS"); break;
    case NAU7802_RATE_320SPS:  Serial.println("320 SPS"); break;
  }
  // Take 10 readings to flush out readings
  for (uint8_t i=0; i<10; i++) {
    while (! nau.available()) delay(1);
    nau.read();
  }
  while (! nau.available()) {
    delay(1);
  }
  int32_t val = nau.read();
  Serial.print("Read "); Serial.println(val);
  ITimer3.init();
  if (ITimer3.attachInterruptInterval(1000, setShouldReadNAU))
  {
    Serial.print(F("Starting  ITimer3 OK, millis() = ")); Serial.println(millis());
  }
  else
    Serial.println(F("Can't set ITimer3. Select another freq. or timer"));
  u8g2.begin();
}

int detectInput() {
  myKeyDetector.detect();
  /* Detecting rotation of the encoder on release rather than push
  (i.e. myKeyDetector.triggerRelease rather myKeyDetector.trigger)
  may lead to more stable readings (without excessive signal ripple) */
  switch (myKeyDetector.triggerRelease) {
    case KEY_A:
      // Signal from Channel A of encoder was detected
      if (digitalRead(ROTARY_EN2) == LOW) {
        // If channel B is low then the knob was rotated CCW
        if (myKeyDetector.current == KEY_C) {
          // If push-button was pressed at that time, then treat this action as GEM_KEY_LEFT,...
          Serial.println("Rotation CCW with button pressed (release)");
          // Button was in a pressed state during rotation of the knob, acting as a modifier to rotation action
          secondaryPressed = true;
          return GEM_KEY_LEFT;
        } else {
          // ...or GEM_KEY_UP otherwise
          Serial.println("Rotation CCW (release)");
          return GEM_KEY_UP;
        }
      } else {
        // If channel B is high then the knob was rotated CW
        if (myKeyDetector.current == KEY_C) {
          // If push-button was pressed at that time, then treat this action as GEM_KEY_RIGHT,...
          Serial.println("Rotation CW with button pressed (release)");
          return GEM_KEY_RIGHT;
          // Button was in a pressed state during rotation of the knob, acting as a modifier to rotation action
          secondaryPressed = true;
        } else {
          // ...or GEM_KEY_DOWN otherwise
          Serial.println("Rotation CW (release)");
          return GEM_KEY_DOWN;
        }
      }
      break;
    case KEY_C:
      // Button was released
      Serial.println("Rotary Button released");
      if (!secondaryPressed) {
        // If button was not used as a modifier to rotation action...
        return GEM_KEY_OK;
      }
      secondaryPressed = false;
      break;
    case KEY_D:
      // Button was released
      Serial.println("Stop Button released");
      return GEM_KEY_CANCEL;
      break;
  }
  switch (myKeyDetector.trigger) {
    case KEY_E:
      Serial.println("End stop 1 pressed");
      break;
    case KEY_F:
      Serial.println("End stop 2 pressed");
      break;
  }
  return GEM_KEY_NONE;
}

int getNAU() {
  return currentNAU;
}
