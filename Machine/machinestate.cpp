#include "machinestate.h"
#include "memory.h"
#include "hardware.h"

MachineState currentState;

void loadMachineState() {
  currentState.operationState = STATE_MAINMENU;
  currentState.loadCellGain = readLoadCellGain();
  currentState.loadCellOffset = readLoadCellOffset();
  currentState.calibrationMass = readCalibrationMass();
}

void calibrateGain() {
  currentState.loadCellGain =
    (9.81 * currentState.calibrationMass)/((float)(getNAU() - currentState.loadCellOffset));
  writeLoadCellGain(currentState.loadCellGain);
}

void calibrateOffset() {
  currentState.loadCellOffset = -1 * getNAU();
  writeLoadCellOffset(currentState.loadCellOffset);
}
