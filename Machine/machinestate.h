#ifndef MACHINESTATE_H
#define MACHINESTATE_H

//#include "memory.h"

#define STATE_MAINMENU (uint8_t)1

struct MachineState {
  int operationState;
  int loadCellGain;
  int loadCellOffset;
  float calibrationMass;
};

extern MachineState currentState;

void loadMachineState();

void calibrateGain();
void calibrateOffset();

#endif
