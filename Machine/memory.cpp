#include "EEPROM.h"
#include "memory.h"

int readLoadCellGain() {
  int r;
  EEPROM.get(0, r);
  return r;
}

void writeLoadCellGain(int gain) {
  EEPROM.put(0, gain);
}

int readLoadCellOffset() {
  int r;
  EEPROM.get(sizeof(int), r);
  return r;
}

void writeLoadCellOffset(int offset){
  EEPROM.put(sizeof(int), offset);
}

float readCalibrationMass() {
  int m;
  EEPROM.get(2 * sizeof(int), m);
  return m;
}

void writeCalibrationMass(int mass){
  EEPROM.put(2 * sizeof(int), mass);
}
