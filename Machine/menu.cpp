#include "hardware.h"
#include "memory.h"
#include "machinestate.h"
#include "menu.h"

unsigned long previousMillis = 0;

byte invert = 1;
SelectOptionByte selectInvertOptions[] = {{"Invert", 1}, {"Normal", 0}};
GEMSelect selectInvert(sizeof(selectInvertOptions)/sizeof(SelectOptionByte), selectInvertOptions);
GEMItem menuItemInvert("Chars order:", invert, selectInvert, applyInvert);

void rawMeasure();
GEMItem menuItemLoadCellRaw("Raw:", rawMeasure);

GEMItem menuItemLoadCellGain("Gain:", currentState.loadCellGain, applyLoadCellGain);
GEMItem menuItemCalibrationMass("Calib. Mass:", currentState.calibrationMass);
void actionCalibrateGain();
GEMItem menuItemCalibrateGain("Calibrate", actionCalibrateGain);

GEMItem menuItemLoadCellOffset("Offset:", currentState.loadCellOffset, applyLoadCellOffset);
void actionCalibrateOffset();
GEMItem menuItemCalibrateOffset("Calibrate", actionCalibrateOffset);

GEMPage menuPageMain("Main Menu"); // Main page
GEMPage menuPageSettings("Settings"); // Settings submenu
GEMPage menuPageCalibration("Calibration"); // Calibration submenu
GEMPage menuPageGain("Gain"); // Calibration submenu
GEMPage menuPageOffset("Offset"); // Calibration submenu

GEMItem menuItemMainSettings("Settings", menuPageSettings);
GEMItem menuItemMainCalibration("Calibration", menuPageCalibration);
GEMItem menuItemCalibrationGain("Gain", menuPageGain);
GEMItem menuItemCalibrationOffset("Offset", menuPageOffset);

GEM_u8g2 menu(u8g2, GEM_POINTER_ROW, GEM_ITEMS_COUNT_AUTO);

void setupMenu() {
  menu.invertKeysDuringEdit(invert);
  
  // Menu init, setup and draw
  menu.init();
  
  // Add menu items to Settings menu page
  menuPageSettings.addMenuItem(menuItemInvert);

  // Add menu items to Calibration menu page
  menuPageCalibration.addMenuItem(menuItemLoadCellRaw);
  menuPageCalibration.addMenuItem(menuItemCalibrationGain);
  menuPageCalibration.addMenuItem(menuItemCalibrationOffset);
  
  menuPageGain.addMenuItem(menuItemLoadCellGain);
  menuPageGain.addMenuItem(menuItemCalibrationMass);
  menuPageGain.addMenuItem(menuItemCalibrateGain);
  
  menuPageOffset.addMenuItem(menuItemLoadCellOffset);
  menuPageOffset.addMenuItem(menuItemCalibrateOffset);

  // Add menu items to menu page
  menuPageMain.addMenuItem(menuItemMainSettings);
  menuPageMain.addMenuItem(menuItemMainCalibration);

  menuPageSettings.setParentMenuPage(menuPageMain);
  menuPageCalibration.setParentMenuPage(menuPageMain);
  menuPageGain.setParentMenuPage(menuPageCalibration);
  menuPageOffset.setParentMenuPage(menuPageCalibration);
  
  // Add menu page to menu and set it as current
  menu.setMenuPageCurrent(menuPageMain);
  menu.drawMenu();
}

void drawMenu() {
  // If menu is ready to accept button press...
  if (menu.readyForKey()) {
    menu.registerKeyPress(detectInput());
  }
}

void applyInvert() {
  menu.invertKeysDuringEdit(invert);
  // Print invert variable to Serial
  Serial.print("Invert: ");
  Serial.println(invert);
}

void applyLoadCellGain() {
  Serial.print("Gain: ");
  Serial.println(currentState.loadCellGain);
  writeLoadCellGain(currentState.loadCellGain);
}

void applyLoadCellOffset() {
  Serial.print("Offset: ");
  Serial.println(currentState.loadCellOffset);
  writeLoadCellOffset(currentState.loadCellOffset);
}

void drawRaw(bool flag) {
  u8g2.firstPage();
  do {
    //u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.drawStr(0,16,"Raw: ");
    
    int measured = getNAU();
    if (measured < 0) {
      u8g2.setCursor(32,16);
      u8g2.print("-");
    }
    u8g2.setCursor(38,16);
    u8g2.print(u8x8_utoa((unsigned int) abs(measured)));
  } while (u8g2.nextPage());
}

void rawContextEnter() {
  // Clear sreen
  u8g2.clear();
  drawRaw(true);
  Serial.println("Printing raw");
}

// Invoked every loop iteration
void rawContextLoop() {
  // Detect key press manually using U8g2 library
  byte key = u8g2.getMenuEvent();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= 100) {
    previousMillis = currentMillis;
    drawRaw(true);
  }
}

// Invoked once when the GEM_KEY_CANCEL key is pressed
void rawContextExit() {
  // Reset variables
  previousMillis = 0;
  // Draw menu back on screen and clear context
  menu.reInit();
  menu.drawMenu();
  menu.clearContext();
}

void rawMeasure() {
  menu.context.loop = rawContextLoop;
  menu.context.enter = rawContextEnter;
  menu.context.exit = rawContextExit;
  menu.context.enter();
}

void actionCalibrateGain() {
  calibrateGain();
  Serial.print("Calibrate Gain: ");
  Serial.println(currentState.loadCellGain);
  menu.drawMenu();
}

void actionCalibrateOffset() {
  calibrateOffset();
  Serial.print("Calibrate Offset: ");
  Serial.println(currentState.loadCellOffset);
  menu.drawMenu();
}
