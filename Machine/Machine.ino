#include <Arduino.h>
#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>
#include <GEM_u8g2.h>
#include <KeyDetector.h>

#include "hardware.h"
#include "memory.h"
#include "machinestate.h"
#include "menu.h"

void setup(void) {
  setupHardware();
  loadMachineState();
  setupMenu();
  Serial.println("Initialized");
}

void loop(void) {
  updateNAU();
  switch (currentState.operationState) {
      case STATE_MAINMENU:
        drawMenu();
        break;
  }
}
