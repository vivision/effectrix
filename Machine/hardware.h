#ifndef HARDWARE_H
#define HARDWARE_H

#include <SPI.h>
#include <Wire.h>
#include <U8g2lib.h>
#include <KeyDetector.h>
#include <U8g2lib.h>

// LCD
#define LCD_SPEAKER 37
#define LCD_CLOCK 23
#define LCD_DATA 17
#define LCD_CS 16

// User inputs
#define ROTARY_EN1    33
#define ROTARY_EN2    31
#define ROTARY_BUTTON 35
#define STOP_BUTTON 41

// End stops

#define END_STOP_1 3
#define END_STOP_2 2

// Signal identifiers for encoder, encoder button, stop button, end stop 1, end stop 2
#define KEY_A 1
#define KEY_B 2
#define KEY_C 3
#define KEY_D 4
#define KEY_E 5
#define KEY_F 6

extern bool secondaryPressed;  // If encoder rotated while key was being pressed; used to prevent unwanted triggers

extern Key keys[];
extern KeyDetector myKeyDetector;

extern int currentNAU;

extern U8G2_ST7920_128X64_1_SW_SPI u8g2;

void setupHardware();
int detectInput();

void updateNAU();
int getNAU();

#endif
