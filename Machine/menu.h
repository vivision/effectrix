#ifndef MENU_H
#define MENU_H

#include <U8g2lib.h>
#include <SPI.h>
#include <Wire.h>
#include <GEM_u8g2.h>
#include <KeyDetector.h>

#include "hardware.h"
#include "memory.h"
#include "machinestate.h"

extern GEM_u8g2 menu;

void setupMenu();
void drawMenu();
void printData();
void applyInvert();
void applyLoadCellGain();
void applyLoadCellOffset();

#endif
