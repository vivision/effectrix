#ifndef MEMORY_H
#define MEMORY_H

struct MachineMemory {
	int loadCellGain;
	int loadCellOffset;
  float calibrationMass;
};

int readLoadCellGain();
void writeLoadCellGain(int gain);
int readLoadCellOffset();
void writeLoadCellOffset(int offset);
float readCalibrationMass();
void writeCalibrationMass(int mass);


#endif
